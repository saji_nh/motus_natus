#require 'bundler/setup'
require 'sidekiq'
require 'sidekiq/api'
require_relative 'workers/create_site.rb'
require_relative 'create_static_site.rb'

Sidekiq.configure_server do |config|
  config.redis = { url: 'redis://localhost:6379'}
end
Sidekiq.configure_client do |config|
  config.redis = { url: 'redis://localhost:6379'}
end
