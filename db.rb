require 'sequel'

# app_dir is also defined in config/puma/production.rb
# please make sure the definitions are the same

user = "motus_natus"
root = "/home/#{user}"
$app_dir = root

DB = Sequel.sqlite(File.join($app_dir,'motus_natus.db')) # requires sqlite3
Model = Class.new(Sequel::Model)
Model.db = DB
Model.plugin :forme

DB.create_table?(:videos) do
  primary_key :id
  String :title,         :null=>false
  String :url,           :null=>false, :unique=>true
  String :provider,      :null=>false
  String :url_thumb_sml, :null=>true
  String :url_thumb_med, :null=>true
  String :url_thumb_lrg, :null=>true
  Date   :date_added,    :null=>false
  Date   :date_shot,     :null=>true
  String :image_data
  String :original_url
  # video.image_url stores location of video
  String :tags
  String :embed_code, :text=>true
end

DB.create_table?(:the_categories) do
  primary_key :id
  String      :name, :unique=>true # name of category
  String      :description
end

DB.create_table?(:the_subcategories) do
  primary_key :id
  String      :name
  Integer     :cid # cid == the_categories.instance.id 
  String      :description
end

DB.create_table?(:the_links) do
  primary_key :id
  Integer :scid    # scid == the_subcategories.instance.id
  Integer :vid     # vid  == videos.instance.id
end

#DB.create_table?(:pages) do
#  primary_key :id
#  String :title    # title of the page
#  String :content, :text=>true
#end
