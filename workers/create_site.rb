class CreateSiteWorker
  include Sidekiq::Worker
  sidekiq_options retry: 0

  def perform
    create_site 
  end
end
