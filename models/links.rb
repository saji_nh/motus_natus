class TheLinks < Sequel::Model(:the_links)
  def self.link_id(scid,vid)
    self.find(scid: scid, vid: vid)
  end
  def self.add(scid,vid)
    lnid = link_id(scid,vid)
    if lnid
      res=vid
    else
      self.new(scid: scid, vid: vid).save
    end
  end
  def self.unlink(scid,vid)
    lnid = link_id(scid,vid)
    lnid.delete if lnid
  end
  def self.sub_category_of_video(category, vid)
    sel_subcat_name=nil
    subcat_ids = category.subcategories.map(&:subcat_id)
    cat_id = category.cat_id

    subcat_ids.each do |subcat_id|
      lnk_id = TheLinks.find(     cat_id: cat_id, 
                              subcat_id: subcat_id, 
                                  vid: vid)
      sel_subcat_name = TheList.find(id: lnk_id.subcat_id).name if lnk_id 
    end
    sel_subcat_name
  end
  def self.links_to_video(vid)
     TheLinks.where(vid: vid)
  end
  def self.video_associations(vid)
    # Finds all scids associated with the video
    #  the searches TheSubCategories with that scid
    # to establish the Category and SubCategory of
    # the video
     self.links_to_video(vid).map(&:scid).
     map {|scid| scat=TheSubCategories.find(id: scid)
     [scat.cid, scat.id]}
  end
end
