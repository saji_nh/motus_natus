class TheCategories < Sequel::Model(:the_categories)
  include TitleHelpers

  def self.add(cat_name,cat_desc)
    process_title!(cat_name)
    process_title!(cat_desc) if cat_desc

    # now save to the_categories
    cid = self.find(name: cat_name)
    if cid
      res=cat_name
    else
      self.new(name: cat_name,  description: cat_desc).save
    end
    # nhs 14 apr 2021; add a default subcategory "None"
    # while creating the category
    # first find cid
    cid = self.find(name: cat_name).id
    # next use cid to create subcategory
    subcategory = TheSubCategories.add(cid,"None","For videos that have no subcategory")

    res
  end
  def delete_or_update(action,cat_name=nil,cat_desc=nil)

    # xxxxxxxxxxx
    #  Important : update also links in TheLinks
    # -----------
    process_title!(cat_name)
    process_title!(cat_desc) if cat_desc

    if action=="update"
      self.update(name: cat_name, description: cat_desc)
    end

    # remove subcategories and links associated with self
    subcats=self.subcategories
    links=subcats.map {|s| s.links}
    if action=="delete"
      subcats.each do |subcat|
        subcat.delete 
      end
      links.each do |link|
        link.delete
      end
      self.delete
    end
  end
  def subcategories
    TheSubCategories.where(cid: self.id)
  end
  def videos
    subcategories.map {|s| s.videos.reverse}
  end
  def subcategory_of_video(vid)
    return nil if TheLinks.count==0
    scats=subcategories.map {|s| TheLinks.where(scid: s.id, vid: vid)}.first
    if scats
      scats.map {|s| TheSubCategories[s.scid].name}.first
    end
    scats
  end
  def associate(subcat_name,vid)
    scid=TheSubCategories.find(name: subcat_name, cid: self.id)
    TheLinks.add(scid.id,vid)
  end
end
