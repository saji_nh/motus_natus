require 'shrine'
require "shrine/storage/file_system"
 
Shrine.storages = { 
  cache: Shrine::Storage::FileSystem.new("public", prefix: "uploads/cache"), # temporary 
  store: Shrine::Storage::FileSystem.new("public", prefix: "uploads"),       # permanent 
}
 
Shrine.plugin :sequel # or :activerecord 
Shrine.plugin :cached_attachment_data # for retaining the cached file across form redisplays 
Shrine.plugin :restore_cached_data # re-extract metadata when attaching a cached file 
Shrine.plugin :rack_file # for non-Rails apps 
Shrine.plugin :determine_mime_type
Shrine.plugin :validation_helpers
Shrine.plugin :remove_invalid
Shrine.plugin :add_metadata


class ImageUploader < Shrine
  Attacher.validate do
    validate_max_size      2*1024*1024
    validate_mime_type %w[image/jpeg image/png image/gif]
    validate_extension %w[jpg jpeg png gif]
#    validate_width 100..500
  end
end

class Video < Model
  include ImageUploader::Attachment(:image) # adds an `image` virtual attribute 
  def remove
    self.links.each do |link|
      link.delete
    end
    self.delete
  end
  def fill_video_info(url)
    res=inspect_video_attributes(url)
    self.url = url
    self.date_added=Date.today
    return self if res==false
    self.title=res[0] if res[0]
    date=res[1]
    self.date_shot=Date.new(date.year,date.month,date.day) if date
    self.provider=res[2] if res[2]
    self.url_thumb_sml=res[3] if res[3]
    self.url_thumb_med=res[4] if res[4]
    self.url_thumb_lrg=res[5] if res[5]
    self.embed_url=res[6] if res[6]
    self.embed_code=res[7] if res[7]
  end
  def associations
    self.video_associations.map {|l| [TheCategories[l.first].name,TheSubCategories[l.last].name]}
  end
  def categories
    TheLinks.categories_of_video(self.id)
  end
  def links
    TheLinks.links_to_video(self.id)
  end
  def video_associations
    TheLinks.video_associations(self.id)
  end
end
