class TheSubCategories < Sequel::Model(:the_subcategories)
  include TitleHelpers

  def self.add(cid,subcat_name,subcat_desc)
    process_title!(subcat_name)
    process_title!(subcat_desc) if subcat_desc

    # now save to the_categories
    scid = self.find(name: subcat_name, cid: cid)
    res=nil
    if scid
      res=subcat_name
    else
      self.new(name: subcat_name, 
                cid: cid, 
        description: subcat_desc).save
    end
    res
  end
  def delete_or_update(category,action,subcat_name=nil,subcat_desc=nil)
    # collect all entries associated to this category
    # delete category 
    process_title!(subcat_name)
    process_title!(subcat_desc) if subcat_desc
    if action=="update"
      self.update(name: subcat_name, cid: category.id,
           description: subcat_desc)
    end
    links = self.links
    if action=="delete"
      links.each do |link|
        link.delete
      end
      self.delete
    end
  end
  def links
    TheLinks.where(scid: self.id)
  end
  def category
    TheCategories.find(id: self.cid)
  end
  def videos
    vids=TheLinks.where(scid: self.id).
                                map {|l| Video.find(id: l.vid)}
    vids.reverse
  end
end
