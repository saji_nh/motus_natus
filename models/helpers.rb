module TitleHelpers
  module ClassMethods
    def process_title!(title)
      title.strip!         # remove leading/trailing spaces
      title.squeeze!(" ")   # only one space between words
    end
  end

  def self.included(base)
    base.extend(ClassMethods)
  end
  include ClassMethods
end
