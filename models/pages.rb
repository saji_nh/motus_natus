def page_path
	File.expand_path("../public", File.dirname(__FILE__))
end

def read_page(title)
  page_fil = File.join(page_path,title+".yml")
  return YAML.load_file(page_fil) if File.exist? (page_fil)
  # the controller (/pages/*) restricts which pages can be created
  # therefore, we don't restrict it here.
  Page.new(title)
end

def write_page(page)
  fnm = File.join(page_path,page.title+".yml")
  File.open(fnm,"w") {|file| file.write(page.to_yaml)}
end

class Page
  attr_reader :title, :content
  def initialize(title)
    @title = title
    @content = nil
  end
 
  def content=(content)
    @content=content
  end
end
