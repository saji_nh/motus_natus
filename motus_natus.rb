require "roda"
require "forme"
require "tilt/erubi"
require 'yaml'
require 'pp'

require_relative './lib/get_video_info.rb'
require_relative 'models.rb'
require_relative 'workers.rb'

class MotusNatus < Roda
  plugin :forme
  plugin :public
  plugin :exception_page
  plugin :error_handler do |e|
    next exception_page(e)
    #File.read('public/500.html')
  end
  plugin :render#, escape: true
  plugin :sessions, secret: ENV.delete('APP_SESSION_SECRET')
  plugin :route_csrf
  plugin :flash
  plugin :typecast_params
  plugin :partials


  route do |r|
    check_csrf!
    r.on "roda" do
      r.public
      r.is "create_site" do
        #create_site 
        # does not work for old Redis Sidekiq::Queue.new.clear
	CreateSiteWorker.perform_async
        "<h2>Please check http://enformtk.u-aizu.ac.jp/motus_natus after 5 mins </h2>"
        #r.redirect "/motus_natus"
      end
      r.is "jobs" do
        jobs = Sidekiq::Queue.new.size
	"#{jobs}"
      end
      r.is "push_site" do
        #push_site 
        "this does not work for the time being"
      end

      r.on "pages" do
        #puts "#{r}"
        %w[about links].each do |route_name|
          r.get(route_name) do
            @page = read_page(route_name)
            view("pages/page")
          end
          r.post(route_name) do
            @page = read_page(route_name)
            @page.content=r.params['content']
            write_page(@page)
            r.redirect "/roda/pages/#{route_name}"
          end
        end
        r.get { "nothing to show here"}
      end # pages

      r.on "videos" do

        r.get do
          r.is "new" do
            @video=Video.new
            view "videos/new"
          end
          r.is Integer,String do |vid,action|
            if (["edit","categorize","delete","show"].any? {|s| s==action})
              @categories=TheCategories.all
              @video = Video[vid]
              view "videos/#{action}"
            end
          end
          r.is do
            next unless @videos=Video.all
            view "videos/index"
          end
        end #get

        r.post do
          r.is "fill" do
            @video=Video.new
            @video.fill_video_info(r.params["url_scraper"].chomp)
            view "videos/new"
          end
          r.is "create" do
            video = Video.new(r.params["video"])
            video.save
            @video = Video.last
            @categories = TheCategories.all
            view "videos/categorize"
          end
          r.is Integer,String do |vid,action|
            if (["update","associate","delete"].any? {|s| s==action})
              @video=Video[vid]
              @video.update(r.params["video"]) if action=="update"
              if action=="associate"
                TheCategories.each do |category|
                  subcat_name=typecast_params.nonempty_str(category.name)
                  if subcat_name
                    category.associate(subcat_name, vid)
                  end
                end
              end
              @video.remove if action=="delete"

            r.redirect "/roda/videos"
            end
          end
        end #post

      end #end_of "videos"

      r.on "categories" do
        r.get do
          r.is "new" do
            view "categories/new"
          end
          r.is Integer, String do |cid,action|
            if (["edit","delete","subcategories"].any? {|s| s==action})
              @category = TheCategories[cid]
              view "categories/#{action}"
            end
          end
          r.is Integer, "subcategories", Integer, String do |cid,scid,action|
            if (["edit","delete","videos","unlink_video"].any? {|s| s==action})
              @category = TheCategories[cid]
              @subcategory = TheSubCategories[scid]
              @vid = r.params["vid"] if r.query_string
              view "categories/subcategories/#{action}"
            end
          end
          r.is do
            next unless @categories=TheCategories.all
            view "categories/index"
          end
        end #get
    
        r.post do
          r.is "create" do
            res=[]
            (1..5).to_a.each do |i|
              cat_name=typecast_params.nonempty_str("category#{i}")
              cat_desc=typecast_params.nonempty_str("category_description#{i}")
              if cat_name
                res<<TheCategories.add(cat_name,cat_desc)
              end
            end
            res.compact! # remove nil values
            flash['error']="categories #{res.join(",")} already taken" unless res.empty?
            r.redirect "/roda/categories"
          end
          r.is Integer, String do |cid,action|
            if (["update","delete"].any? {|s| s==action})
              @category = TheCategories[cid]
              cat_name=typecast_params.nonempty_str("name")
              cat_desc=typecast_params.nonempty_str("description")
              @category.delete_or_update(action, cat_name, cat_desc) 
              r.redirect "/roda/categories"
            end
          end
          r.is Integer, "subcategories","create" do |cid|
            "hey #{cid} #{r.params}"
            @category = TheCategories[cid]
            res=[]
            (1..5).to_a.each do |i|
              subcat_name=typecast_params.nonempty_str("subcat#{i}")
              subcat_desc=typecast_params.nonempty_str("subcat_desc#{i}")
              if subcat_name
                res<<TheSubCategories.add(cid,subcat_name,subcat_desc)
              end
            end
            view "categories/subcategories"
          end
          r.is Integer, "subcategories", Integer, String do |cid,scid,action|
            if (["delete","update","unlink_video"].any? {|s| s==action})
              @category = TheCategories[cid]
              @subcategory = TheSubCategories[scid]
              subcat_name=typecast_params.nonempty_str("name")
              subcat_desc=typecast_params.nonempty_str("description")
              if action=="unlink_video"
                vid = r.params["vid"] if r.query_string
                TheLinks.unlink(scid,vid)
              else
                @subcategory.delete_or_update(@category,
                              action,subcat_name,subcat_desc) unless subcat_name.nil?
              end
              r.redirect "/roda/categories/#{cid}/subcategories"
            end
          end
        end #post
      end

    end # end of "roda"
  end # end of whole routing
end # end of class
