
# modify this to indicate the root directory
# under which the application directory 'motus_natus' exists

user = "motus_natus"
#root = File.join("#{ENV['HOME']}",'Software')
root = "/home/#{user}"

#  --- no need to modify after this line ----

app_dir = root
bind  "tcp://0.0.0.0:9292"
#port 9292
pidfile "#{app_dir}/puma.pid"
state_path "#{app_dir}/puma.state"
directory "#{app_dir}/"
stdout_redirect "#{app_dir}/log/puma.stdout.log", "#{app_dir}/log/puma.stderr.log", true
workers 2
threads 1,5
activate_control_app
prune_bundler
