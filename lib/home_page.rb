require 'erubis'
require_relative 'site_helpers'

class HomePage
#  include ERB::Util
  include SiteHelpers
  attr_accessor :title, :description, :template
  attr_accessor :categories, :subcategories, :all_categories
  attr_accessor :videos, :video
  attr_accessor :all_vid_count
  attr_accessor :category, :subcategory
  attr_accessor :static_page, :site_css

  def initialize(title=nil,description=nil)
    @page_title = title
    @page_description = description
    @template = template
    @all_categories=categories
    @categories=categories
    @category=category
    @subcategories=subcategories
    @subcategory=subcategory
    @videos=videos
    @video=video
    @all_vid_count=all_vid_count
  end

  def render
    Erubis::Eruby.new(@template,:trim=>true).result(binding).strip
  end

  def save(file)
    File.open(file, "w+") do |f|
      f.write(render)
    end
  end
end
