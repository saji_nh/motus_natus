require "image_processing/mini_magick"
require "fileutils"

module SiteHelpers
  module ClassMethods

    def clear_cache
      file_system = Shrine.storages[:cache]
      file_system.clear! {|path| path.mtime < Time.now - 7*24*60*60}
    end
    def thumb_med_size
      [320, 180]
    end
    def static_site
      "#{$app_dir}/static_site/src"
    end
    def thumbnail_dir
      "/images/thumbnails"
    end
   def site_thumbnail_dir
      File.join(static_site,thumbnail_dir)
    end
    def make_blank_png(filnam)
#      p "making blank image"
      MiniMagick::Tool::Convert.new do |i|
        i.size thumb_med_size.join("x")
        i.gravity "center"
        i.xc "black"
        i.caption "blank image"
        i << filnam
      end
    end
    def blank_image
      outfil = File.join(site_thumbnail_dir,"blank.png")
      relocatable_outfil = "{relocatable: #{File.join(thumbnail_dir,File.basename(outfil))}}"
      return relocatable_outfil if File.exist? outfil
      blank_png = File.join(File.dirname(__FILE__),"../share/blank.png")
      make_blank_png(blank_png) unless File.exists?(blank_png)
      FileUtils.mkdir_p(site_thumbnail_dir)
      FileUtils.cp(blank_png, outfil)
      "{relocatable: #{File.join(thumbnail_dir,File.basename(outfil))}}"
    end

    def make_slug(label)
      label.split(" ").join("_").downcase
    end

    def youtube_embed(url)
      parts=url.split("/")
      parts[2]="youtube.com/embed"
      parts.join("/")
    end
    def get_thumbnail(vid)
      #clear_cache
      return blank_image unless vid.valid?
      return blank_image if (vid.image && vid.image.mime_type.match(/mp4/))
      return blank_image if (vid.image && vid.image.mime_type.match(/bmp/))
      return resized_thumbnail(vid) if vid.image_url
      return vid.url_thumb_med if !vid.url_thumb_med.empty?
      blank_image # return blank_image if no thumbnail found
    end
    def org_thumbnail(vid)
      fnam = File.join("#{$app_dir}/public",vid.image_url) 
      return fnam
    end
    def resized_thumbnail(vid)
      thumbnail = org_thumbnail(vid)
      outfil = File.join(thumbnail_dir,File.basename(thumbnail))
      relocatable_outfil = "{relocatable: #{outfil}}"
      return relocatable_outfil if File.exist? File.join(static_site,outfil)
      #puts "we just processed #{outfil}"
      processed = ImageProcessing::MiniMagick
         .source(thumbnail)
         .resize_to_fill(*thumb_med_size)
         .strip
         .call
      #site_thumbnail_dir = File.join(static_site,thumbnail_dir)
      FileUtils.mkdir_p(site_thumbnail_dir)
      FileUtils.cp(processed.path, File.join(static_site,outfil))
      relocatable_outfil
    end
  end

  def self.included(base)
    base.extend(ClassMethods)
  end
  include ClassMethods
end
