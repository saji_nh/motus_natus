# Motus Natus

This repository contains:

 1. An administrative interface to manage a database for video collections.
 2. Software that will create static webpages that can be used to
    stream the videos

## First thing to do
 
Do a 'bundle install' so that all the necessary software is installed.

## Administrative interface

You can add metadata about videos hosted elsewhere, such as LBRY, YouTube,
Vimeo etc. The metadata include urls for the video, title, thumbnails,
embed code etc. You can also upload custom thumbnails. 
The interface also allows you to create categories and subcategories and have the videos
classified under these.

The administrative interface was created using [roda](https://roda.jeremyevans.net), 

Being a small project, we use the sqlite database to store the data. However,
any database supported by the [sequel gem](https://sequel.jeremyevans.net) can be used.

### Running the admin interface

To run the admin interface, please edit 'db.rb' and modify the variable
root to indicate the base directory where this software resides.

You should also edit config/puma/production.rb. This configuration file is used
when the software is run as a system service. 

Note that puma is used as the application server, and most modern linux systems
will not allow it to be run as a background process from outside. So, you will
need to run it as a system service. Please see extra/motus_natus.service for
an example configuration, and extra/nginx.conf for the essential configuration
needed to connect nginx to the puma server.

## Building the static website

To prepare the static website, we follow a  two-step procedure:

1. We pull video metadata from the database created by the administrative interface
  above and create page fragments using a custom ruby script 'create_static_site.rb'.
  
   Run 'ruby create_static_site.rb'.
   These page fragments are stored under static_site/src

2. In the second step, we use the static website building tool 
[webgen](https://webgen.gettalong.org) to compile these into a static website.

  To create the website, chdir to static_website and then run 'webgen'

The website can be served using a server such as nginx or Apache
