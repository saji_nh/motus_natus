require 'fileutils'
require 'stringex' # useful to make slugs
require 'kramdown'
require 'base64'

require_relative 'models.rb'
require_relative 'lib/home_page.rb'
require_relative 'lib/site_helpers.rb'

include SiteHelpers

def mkdir(dir_name)
  FileUtils.mkdir_p(dir_name)
end

def push_site
#  Dir.chdir(File.join(File.dirname(__FILE__),"static_site")) do
#    `rsync -av out shortxxvid@saji.nh.pserver.ru:./`
#  end
end

def default_template(src,site_dir,fil,site_css)
  site_css_name = "site."+Base64.encode64(Time.now.to_s)[0..-4]+".css"
  template = IO.readlines(File.join(src,fil+".head"))
  page = HomePage.new("Template","the template page")
  page.template=template.join
  page.site_css = site_css_name
  tail = File.read(File.join(src,fil+".tail"))
  home_path = File.join(site_dir,"default.template")
  puts "copying default template"
  File.open(home_path, "w+") do |f|
    f.write(page.render)
    f.write(tail)
  end
  # rm existing css files in tgt_dir
  FileUtils.rm Dir.glob(File.join(site_dir,'css/site.*.css')), :verbose=>true
  FileUtils.rm Dir.glob(File.join(site_dir,'../out/css/site.*.css')), :verbose=>true
  css_path = File.join(site_dir,'css',site_css_name)
  File.open(css_path, "w+") do |f|
    f.write(site_css)
  end
end

def compile_template(src,tgt,fil)
  site_css = File.read(File.join(src,'css','site.css'))
  default_template(src,tgt,fil,site_css)
end

def copy_it(src,tgt,fil,compile=false)
  return compile_template(src,tgt,fil)	if compile

  src_fil = File.join(src,fil)
  tgt_fil = File.join(tgt,fil)
  if  !File.exist? tgt_fil
    FileUtils.cp_r(src_fil,tgt_fil, :noop=>false, :verbose=>true) 
    return
  end
  FileUtils.cp_r(src_fil,tgt_fil, :noop=>false, :verbose=>true) if File.mtime(src_fil) > File.mtime(tgt_fil)
end

def copy_static_assets(src,tgt)
  img_tgt = "#{tgt}/images"
  copy_it(src,img_tgt,'three_dots.png')
  copy_it(src,tgt,'css')
  copy_it(src,tgt,'default.template',compile=true)
end

def create_site
  copy_static_assets(File.expand_path("./share"),File.expand_path("./static_site/src"))
  cats= []
  subcats = []
  homecat = []
  TheCategories.each do |category|

   # choose only categories that contain videos
  vcount = category.videos.flatten.count
  if vcount > 0
              # choose only if there are videos
    if category.name.upcase.strip == "HOME"
      homecat << category
      homecat << category.subcategories.
            map {|s| s if (s.videos.flatten.count > 0) }.compact
    end
    cats << category
              # choose subcats having one or more videos
    subcats << category.subcategories.
          map {|s| s if (s.videos.flatten.count > 0) }.compact
       
  end
end

all_vid_count = Video.all.count

p "home category created" 

site_dir = File.expand_path("./static_site/src")
# Step 1 - Build the home page to show:
#        - category names
#        - links to each category page
#        - a list of 20 recent videos

template = IO.readlines(File.expand_path("./share/index.template"))
page = HomePage.new("Featured Playlists",homecat[0].description)
page.template=template.join
page.all_categories = cats
page.categories = homecat[0] #.map{ |c| c.name}
page.subcategories = homecat[1] #.map{ |c| c.name}
page.videos = page.subcategories.map { |scat| scat.videos}
#all_vid_count = cats.map {|c| c.videos}.flatten.count
page.all_vid_count = all_vid_count
home_path = File.join(site_dir,"index.page")
page.save(home_path)

# the 3 lines below are just for testing
#page.videos.flatten.each_with_index do |video,i|
#  abort get_thumbnail(video) if video.image_url
#end
# -- testing over

# About page
page = HomePage.new("About","description")
page.categories = cats #.map{ |c| c.name}
page.videos = cats.map {|c| c.videos}
page.template=IO.readlines("./share/about.template").join
page.static_page = read_page("about")
page.save(File.join(site_dir,"about.page"))

# Links page
page = HomePage.new("Links","description")
page.categories = cats #.map{ |c| c.name}
page.videos = cats.map {|c| c.videos}
page.template=IO.readlines("./share/links.template").join
page.static_page = read_page("links")
page.save(File.join(site_dir,"links.page"))

# make subdirectories to hold subcategories
cat_template = IO.readlines("./share/category.template")
subcat_template = IO.readlines("./share/subcategory.template")
subcat_video_template = IO.readlines("./share/video.template")
cats.each_with_index do |cat,cid|

  subcat = subcats[cid]
  cat_name = cat.name
  page = HomePage.new(cat.name,cat.description)
  page.template=cat_template.join
  page.categories = cats
  page.category = cat_name
  page.all_vid_count = all_vid_count
  page.subcategories = subcats[cid] #.map{ |c| c.name}
  page.videos = page.subcategories.map { |scat| scat.videos}

  page_path = File.join(site_dir,cat.name.to_url)
  p cat_name
  mkdir(page_path)
  page_index = File.join(page_path,"index.page")
  page.save(page_index)

  # make subdirectories to hold videos
  page.subcategories.each_index do |scid|

    subcat_name = page.subcategories[scid].name
    subcat_page = HomePage.new(subcat_name,page.subcategories[scid].description)
    subcat_page.template=subcat_template.join
    subcat_page.videos = page.videos[scid]
    subcat_page.categories = cats
    subcat_page.category = cat_name
    subcat_page.all_vid_count = all_vid_count

    p subcat_name

    subcat_page_path = File.join(page_path,subcat_name.to_url)
    mkdir(subcat_page_path)
    subcat_page_index = File.join(subcat_page_path,"index.page")
    subcat_page.save(subcat_page_index)

    subcat_page.videos.each do |video|

      subcat_video_name = video.title
      subcat_video_page = HomePage.new(subcat_video_name,"video.description")
      subcat_video_page.template=subcat_video_template.join
      subcat_video_page.video = video
      subcat_video_page.videos = subcat_page.videos
      subcat_video_page.categories = cats
      subcat_video_page.subcategory = subcat_name
      subcat_video_page.category = cat_name
      subcat_video_page.all_vid_count = all_vid_count
    
      subcat_video_page_path = File.join(subcat_page_path,video.title.to_url)
      mkdir(subcat_video_page_path)
      subcat_video_page_index = File.join(subcat_video_page_path,"index.page")
      subcat_video_page.save(subcat_video_page_index)
    end
  end
end
Dir.chdir(File.join(File.dirname(__FILE__),"static_site")) do
  `webgen`
end
end
